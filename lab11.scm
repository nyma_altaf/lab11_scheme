;Definition Of Extra Functions Used In Code

;Equals
(define EQ (lambda (n) (AND (LEQ m n) (LEQ n m))))

;OR
(define OR (lambda (M N) (lambda (a b) (N a (M a b)))))

;LT
(define LT (lambda (a b) ((AND( (NOT(EQ a b)) (LEQ a b) )))))

;ADD 
(define ADD (lambda (m n s z)(m s(n s z))))

;IsZero
(define isZero (lambda (n) (n (lambda(x) (false)) true)))

;Definitions for forloop

;Fixpoint Combinator
(define Y (lambda (f) ((lambda(x) f(x x)) (lambda(x) f(x x)))))

;Recursive function using if else for for-loop
(define forloop_func (lambda (n f) ((IsZero n) n (ADD n f(n-1)))))

;Definition for the C function

(define aCompletelyUselessFunctionCreatedOnlyToMakeYourLifeMiserable
        (lambda (m n) (OR((IsZero m) (LT m n)) (Y forloop_func(n forloop_func)) (ADD n m))))


